# vmol-s3

Moleculer mixin adding s3 access to the local service.

## Configuration

Settings can be provided by the service or using enviroment variables.

```
settings: {
    s3: {
        keyId: process.env.S3_STORAGE_KEYID,
        key: process.env.S3_STORAGE_KEY,
        region: process.env.S3_STORAGE_REGION,
        endpoint: process.env.S3_STORAGE_ENDPOINT,
        container: process.env.S3_STORAGE_CONTAINER,
        delimiter: process.env.S3_STORAGE_DELIMITER
    }
}
```

## Usage

To enable, add the middleware to your services.

```javascript
const S3Service = require('vmol-s3');

module.exports = {
    name: 'myservice',

    /**
     * Mixins
     */
    mixins: [ S3Service ],
};

```

## API

### s3upload(stream, options)
### s3uploadFromRequest(req, options)
### s3stat(name, options)
### s3getStream(name, offset, length, options)
### s3getBuffer(name, options)
### s3streamToKoa(ctx, name, options)
### s3getTag(source, options)
### s3move(source, destination, options)
### s3copy(source, destination, options)
### s3listFiles(names, options)
### s3remove(name, options)
### s3removeFiles(names, options)

### s3addClient(name, options)
### s3withStaticUrl(prefix, data, fields)

## Advanced configuration

### Multiple containers in a single client connection

You may need access to more than one container. In this case, you can specify it
in all API methods, inside the `options` object.

```javascript
this.s3upload(stream, { container: 'other-container' });

```

If omitted, the `settings.s3.container` value will be used.

### Multiple client connections

You may need to work with different client connections to different buckets. Behind the
scene, the s3 settings will be used to create a specific connection.

You can add a new client connection this way:

```javascript
// best practice is to add your client in started() moleculer method
started() {
    this.s3addClient('some-name', {
        keyId: process.env.PRIVATE_STORAGE_KEYID,
        key: process.env.PRIVATE_STORAGE_KEY,
        region: process.env.PRIVATE_STORAGE_REGION,
        endpoint: process.env.PRIVATE_STORAGE_ENDPOINT,
        container: process.env.PRIVATE_STORAGE_CONTAINER
    });
}

// upload with the new specific client
this.s3upload(stream, { client: 'some-name' });

// upload with main client using settings.s3
this.s3upload(stream);
// or explicitly
this.s3upload(stream, { client: '' }); // or any falsy value
```

#### Mulitple containers in a specific client connection

You can specify both client and container if you have a complex set of connections
and containers.

```javascript
this.s3upload(stream, {
    client: 'some-name',
    container: 'some-container'
});
```

By default, it is always the container defined in client connection configuration
which id used.
