'use strict';

const { MoleculerServerError, MoleculerClientError } = require('moleculer').Errors;
const busboy = require('busboy');
const Minio = require('minio');
const lodash = require('lodash');
const crypto = require('crypto');
const toArray = require('stream-to-array');
const mimeESM = () => import('mime');

/**
 * Object storage Service definition
 */
module.exports = {
    /**
     * Settings
     */
    settings: {
        s3: {
            keyId: process.env.S3_STORAGE_KEYID,
            key: process.env.S3_STORAGE_KEY,
            region: process.env.S3_STORAGE_REGION,
            endpoint: process.env.S3_STORAGE_ENDPOINT,
            container: process.env.S3_STORAGE_CONTAINER,
            delimiter: process.env.S3_STORAGE_DELIMITER,
            filepathMaxLength: process.env.S3_STORAGE_FILEPATH_MAX_LENGTH
        }
    },

    /**
     * Methods
     */
    methods: {
        /**
         * Add static storage url prefix. Internally, uses lodash.get. To add
         * static url to sub arrays, you can use this in fields arg:
         * ['items[*].href'], where "items" must be an array containing objects
         * with "href" property.
         *
         * You can use wildcard for properties: ['status.*.href']. You can of
         * course combine array and object wildcards.
         *
         * You can do complex wildcard combination, like ['*[*].link'].
         *
         * @note This method mutates the given objet|array
         *
         * @param {String|Function} prefix Prefix to add before url or function with args [object|array], [key|index] and [value]
         * @param {Object|Array<Object>} data Object(s) containing file url field
         * @param {Array<String>} fields Fields names which contain file url
         *
         * @returns {Object|Array<Object>}
         */
        s3withStaticUrl(prefix, data, fields) {
            let isarray = true;
            if (!lodash.isArray(data)) {
                data = [data];
                isarray = false;
            }
            const arraykey = '[*]';
            const propkey = '.*';

            data.forEach(d => fields.forEach(f => {
                const arraypos = f.indexOf(arraykey);
                const proppos = f.indexOf(propkey);
                const propfirst = f.indexOf('*') === 0;

                // wildcard on arrays
                if (arraypos !== -1 && !propfirst && (proppos === -1 || arraypos < proppos)) {
                    let field, subf;
                    if (arraypos > 0) {
                        // when wildcard applies to a child property
                        const t = f.split(arraykey);
                        field = lodash.get(d, t.shift());
                        subf = lodash.trimStart(t.join(arraykey), '.');
                    } else {
                        // when wildcard applies to parent array
                        field = d;
                        subf = lodash.trim(f.replace(arraykey, ''), '.');
                    }
                    if (!lodash.isArray(field)) {
                        return;
                    }
                    return subf
                        // object properties inside array
                        ? field.forEach(sub => this.s3withStaticUrl(prefix, sub, [subf]))
                        // simple array of strings
                        : field.forEach((sub, i) => sub ? (lodash.isFunction(prefix) ? prefix(field, i, sub) : field[i] = prefix + sub) : sub);
                }

                // wilcard on properties
                if (proppos !== -1 || propfirst) {
                    let field, subf;
                    if (!propfirst) {
                        // when wildcard applies to child property
                        const t = f.split(propkey);
                        field = lodash.get(d, t.shift());
                        subf = lodash.trimStart(t.join(propkey), '.');
                    } else {
                        // when wildcard applies to parent object
                        field = d;
                        subf = lodash.trim(f.replace('*', ''), '.');
                    }
                    if (!lodash.isObject(field)) {
                        return;
                    }
                    return subf
                        // sub object inside object
                        ? this.s3withStaticUrl(prefix, field, Object.keys(field).map(sub => `${sub}${subf.indexOf('[') === 0 ? '' : '.'}${subf}`))
                        // all properties of object as simple string
                        : Object.entries(field).forEach(sub => sub[1] ? (lodash.isFunction(prefix) ? prefix(field, sub[0], sub[1]) : field[sub[0]] = prefix + sub[1]) : sub[1]);
                }

                // standard case
                const value = lodash.get(d, f);
                value && (lodash.isFunction(prefix) ? prefix(d, f, value) : lodash.set(d, f, prefix + value));
            }));
            return isarray ? data : data[0];
        },

        /**
         * Upload an object to the store.
         *
         * @param {Stream} stream Object's datastream.
         * @param {Object} options Object
         * @param {String} options.name File name on storage
         * @param {String} options.contentType File mime type
         * @param {Boolean} options.sanitize Sanitize the filename before upload. default: true
         * @param {Boolean} options.randomize Randomize the upload path. default: false
         * @param {String} options.path Subpath to upload the file to, will be put before the random and name.
         * randomize
         * @param {Number} [options.size] File size in bytes
         * @param {String} [options.client] S3 client used
         * @param {String} [options.container] S3 container used
         *
         * @returns {Object} Object's metadata.
         */
        s3upload(stream, options) {
            return this._s3uploadFromStream(stream, options)
                .catch(this._s3handleUpstreamErrors(options.name, options));
        },

        /**
         * Upload a single object from a Koa request.
         *
         * @param {Object} req http request object
         * @param {Object} options Object
         * @param {Boolean} options.sanitize Sanitize the filename before upload. default: true
         * @param {Boolean} options.randomize Randomize the upload path. default: false
         * @param {String} options.path Subpath to upload the file to, will be put before the random and name.
         * @param {String} [options.client] S3 client used
         * @param {String} [options.container] S3 container used
         * @param {String} [options.limit] Max number of files uploaded. Set -1 for no limit. Default to 1
         *
         * @returns {Object} Object's metadata.
         */
        s3uploadFromRequest(req, options = {}) {
            options.limit = options.limit || 1;
            return this
                ._s3parseFromRequest(req, options.limit, (fieldname, stream, info) => {
                    // called individually for each file
                    const { filename, mimeType } = info;
                    return this
                        .s3upload(stream, {
                            name: filename,
                            contentType: mimeType,
                            ...options
                        })
                        .catch(e => {
                            if (!options.silent) {
                                this.logger.error(`Error while uploading file ${filename} to s3`, e);
                            }
                            throw e;
                        });
                })
                .then(async ({ files: filePromises, fields, filesLimitOverflowed }) => {
                    // because function in parseFromRequest returns Promise, we need to await them
                    const files = await Promise.all(filePromises);

                    if (filesLimitOverflowed) {
                        throw new MoleculerClientError('Too many files given', 400, 'ERR_BAD_REQUEST', {
                            expected: options.limit,
                            actual: files.length
                        });
                    }

                    if (!files.length) {
                        throw new MoleculerServerError('No file given!', 400, 'ERR_BAD_REQUEST', {
                            type: 'nofile',
                            fields
                        });
                    }
                    if (options.limit > 1) {
                        // return files as array
                        return files.map(file => {
                            file.fields = fields;
                            return file;
                        });
                    }

                    // take only first file and add form fields if any
                    const file = files[0];
                    file.fields = fields;

                    return file;
                });
        },

        /**
         * Stat a file and get its metadata.
         *
         * @param {String} name Object's name
         * @param {Object} options
         * @param {String} [options.client] S3 client used
         * @param {String} [options.container] S3 container used
         * @returns {Object} Stat object with `name`, `size`, `contentType`, `lastModified`, `metaData` and `etag`.
         */
        s3stat(name, options = {}) {
            return this._s3stat(name, options)
                .catch(this._s3handleUpstreamErrors(name, options));
        },

        /**
         * Get a download stream for an object.
         *
         * @param {String} name File name
         * @param {Number} offset optional offset to read from
         * @param {Number} length optional length to read
         * @param {Object} options
         * @param {String} [options.client] S3 client used
         * @param {String} [options.container] S3 container used
         *
         * @returns {Promise<Object>} Readable stream and metadata
         */
        s3getStream(name, offset = null, length = null, options = {}) {
            // Stat first, so that we catch errors before sending the stream.
            return this._s3stat(name, options)
                .then(async meta => {
                    const stream = await this._s3getStreamForObject(name, offset, length, options);
                    return { stream, meta };
                })
                .catch(this._s3handleUpstreamErrors(name, options));
        },

        /**
         * Get a buffer for an object
         *
         * @param {String} name File name
         * @param {Object} options
         * @param {String} [options.client] S3 client used
         * @param {String} [options.container] S3 container used
         *
         * @returns {Promise<Object>} Buffer, Readable stream and metadata
         */
        s3getBuffer(name, options = {}) {
            return this.s3getStream(name, null, null, options)
                .then(async data => {
                    const parts = await toArray(data.stream);
                    const buffers = parts.map(part => Buffer.from(part));
                    data.buffer = Buffer.concat(buffers);
                    data.stream.destroy();
                    return data;
                });
        },

        /**
         * Stream an object directly to Koa's response
         *
         * Will use the etag and last-modified headers and stream only if they don't match by default.
         *
         * @param {Object} ctx Koa context
         * @param {String} name File name
         * @param {Object} options
         * @param {Boolean} options.noCache Force streaming even if object is not modified
         * @param {String} [options.client] S3 client used
         * @param {String} [options.container] S3 container used
         *
         * @returns {Promise}
         */
        s3streamToKoa(ctx, name, options) {
            options = options || {};

            return this.s3getStream(name, null, null, options)
                .then(res => {
                    ctx.set('Content-Type', res.meta.contentType || 'application/octet-stream');

                    if (res.meta.size) { ctx.length = res.meta.size; }
                    if (res.meta.lastModified) { ctx.lastModified = res.meta.lastModified; }
                    if (res.meta.etag) { ctx.etag = res.meta.etag; }

                    // The client already have the latest object in cache.
                    // Return a 304 and destroy the stream to cancel the data transfer.
                    // @see https://koajs.com/ request.fresh
                    ctx.status = 200;

                    if (!options.noCache && ctx.fresh) {
                        ctx.status = 304;
                        res.stream.destroy();
                        return;
                    }

                    ctx.body = res.stream;
                });
        },

        /**
         * Move an object
         *
         * Internaly it first copy the object and then delete the old one.
         *
         * @param {String} source The object's current name
         * @param {String} destination The object's new name
         * @param {Object} options
         * @param {String} [options.client] S3 client used
         * @param {String} [options.container] S3 container used
         *
         */
        s3move(source, destination, options = {}) {
            return this._s3copy(source, destination, options)
                .then(() => this._s3remove(source, options))
                .catch(this._s3handleUpstreamErrors(source, options));
        },

        /**
         * Copy an object
         *
         * @param {String} source The current object's name
         * @param {String} destination The new object's name
         * @param {Object} options
         * @param {String} [options.client] S3 client used
         * @param {String} [options.container] S3 container used
         */
        s3copy(source, destination, options = {}) {
            return this._s3copy(source, destination, options)
                .catch(this._s3handleUpstreamErrors(source, options));
        },

        /**
         * Remove an object from the store
         *
         * @param {String} name The object's name
         * @param {Object} options
         * @param {String} [options.client] S3 client used
         * @param {String} [options.container] S3 container used
         */
        s3remove(name, options = {}) {
            return this._s3remove(name, options)
                .catch(this._s3handleUpstreamErrors(name, options));
        },

        /**
         * Get a unique tag based (or not) on the stream content
         *
         * @param {Stream} stream
         * @param {Object} options
         * @param {String} [options.client] S3 client used
         * @param {String} [options.container] S3 container used
         * @returns {String}
         */
        s3getTag(stream, options = {}) {
            const rnd = crypto.randomBytes(6)
                .toString('base64')
                .replace(/\+/g, '-')
                .replace(/\//g, '_');

            return rnd.toLowerCase();
        },

        /**
         * List files in the specified folder
         *
         * @param {String} folder
         * @param {Object} options
         * @returns {ReadableStream}
         */
        s3listFiles(folder, options = {}) {
            const client = this._s3getClient(options.client);
            const container = options.container || this._s3getContainer(options.client);
            return client.listObjects(container, folder, true);
        },

        /**
         * List all files via an async function
         *
         * @kind helper function
         * @description Returns an array with all files found. Do not use if lots of files
         * are expected to be found. Use stream directly instead.
         * @param {String} folder
         * @param {Object} options
         * @returns {Promise<Array<String>>}
         */
        s3listFilesAsync(folder, options = {}) {
            return new Promise((resolve, reject) => {
                const files = [];
                const stream = this.s3listFiles(folder, options);

                stream.on('error', async err => {
                    reject(err);
                });
                stream.on('end', async() => {
                    resolve(files);
                });
                stream.on('data', file => {
                    files.push(file.name);
                });
            });
        },

        /**
         * Remove a set of files in one request
         *
         * @param {Array<String>} files
         * @param {Object} options
         * @returns {Promise<void>}
         */
        s3removeFiles(files, options = {}) {
            const client = this._s3getClient(options.client);
            const container = options.container || this._s3getContainer(options.client);

            const promise = new Promise((resolve, reject) => {
                client.removeObjects(container, files, err => {
                    if (err) {
                        return reject(err);
                    }
                    resolve();
                });
            });
            return promise.catch(this._s3handleUpstreamErrors(null, options));
        },

        /**
         * Add a new minio client
         *
         * @param {String} name
         * @param {Object} config
         * @param {String} config.keyId
         * @param {String} config.key
         * @param {String} config.region
         * @param {String} config.endpoint
         * @param {String} config.container
         *
         * @returns {Minio.Client}
         */
        s3addClient(name, config) {
            this.s3clients[name] = new Minio.Client({
                endPoint: config.endpoint,
                region: config.region,
                useSSL: true,
                accessKey: config.keyId,
                secretKey: config.key
            });

            this.s3containers[name] = config.container;
            this.s3delimiters[name] = config.delimiter || '/';

            return this.s3clients[name];
        },

        _s3getClient(name) {
            name = name || this.s3defaultClient;

            if (!this.s3clients[name]) {
                throw new MoleculerServerError(`No s3 client found with name ${name}`, 500);
            }
            return this.s3clients[name];
        },

        _s3getContainer(name) {
            name = name || this.s3defaultClient;

            if (!this.s3containers[name]) {
                throw new MoleculerServerError(`No s3 container found with name ${name}`, 500);
            }
            return this.s3containers[name];
        },

        _s3getDelimiter(name) {
            name = name || this.s3defaultClient;

            if (!this.s3delimiters[name]) {
                throw new MoleculerServerError(`No s3 delimiter found with name ${name}`, 500);
            }
            return this.s3delimiters[name];
        },

        _s3parseFromRequest(req, limit, fileFn) {
            return new Promise((resolve, reject) => {
                const bb = busboy({ headers: req.headers });

                const files = [];
                const fields = {};

                let filesLimitOverflowed = false;

                const onFile = (fieldname, stream, info) => {
                    if (limit > -1 && files.length >= limit) {
                        filesLimitOverflowed = true;
                        stream.resume(); // skip this stream
                        return;
                    }
                    files.push(fileFn(fieldname, stream, info));
                };

                const onField = (fieldname, value) => {
                    fields[fieldname] = value;
                };

                const onFinish = () => {
                    clean();
                    resolve({ files, fields, filesLimitOverflowed });
                };

                const onError = err => {
                    clean();
                    reject(err);
                };

                const clean = () => {
                    // to avoid unexpected leaks
                    bb.removeListener('file', onFile);
                    bb.removeListener('field', onFile);
                    bb.removeListener('error', onError);
                    bb.removeListener('close', onFile);
                };

                bb.on('field', onField);
                bb.on('file', onFile);
                bb.on('error', onError);
                bb.on('close', onFinish);

                req.pipe(bb);
            });
        },

        /**
         * Ensure the right path delimiter is used.
         * And the path is not starting with a delimiter.
         *
         * @param {String} name Object's name
         * @param {Object} options
         * @param {String} [options.client] S3 client used
         *
         * @returns {String}
         */
        _s3sanitizePath(name, options = {}) {
            const delimiter = this._s3getDelimiter(options.client);
            return lodash.trim(
                name.replace(/\//g, delimiter),
                delimiter
            );
        },

        /**
         * Upload an object from a stream
         *
         * @param {Stream} stream
         * @param {Object} options
         * @param {String} options.name File name on storage
         * @param {String} options.contentType File mime type
         * @param {Boolean} options.sanitize Sanitize the filename before upload. default: true
         * @param {Boolean} options.randomize Randomize the upload path. default: false
         * @param {String} options.path Subpath to upload the file to, will be put before the random and name
         * @param {Object} options.metadata Some metadata to add to file on storage
         * @param {Boolean} options.public Shortcut to set public read ACL on file
         * @param {Number} options.maxLength Max length of full path. If null, no max length is applied. default: null
         * @param {Number} [options.size] File size in bytes
         * @param {String} [options.client] S3 client used
         * @param {String} [options.container] S3 container used
         * @return {Promise<Object>}
         */
        _s3uploadFromStream(stream, options) {
            const client = this._s3getClient(options.client);
            const container = options.container || this._s3getContainer(options.client);

            const delimiter = this._s3getDelimiter(options.client);

            let fullPath = options.name;

            if (options.sanitize !== false) {
                fullPath = lodash.deburr(fullPath)
                    .toLowerCase()
                    .replace(/[^a-z0-9_./-]/gi, '');
            }

            if (options.randomize) {
                const rnd = crypto.randomBytes(15)
                    .toString('base64')
                    .replace(/\+/g, '-')
                    .replace(/\//g, '_');

                fullPath = rnd + delimiter + fullPath;
            }

            if (typeof options.path === 'string') {
                fullPath = options.path + delimiter + fullPath;
            }

            fullPath = this._s3sanitizePath(fullPath, options);

            const maxLength = options.maxLength || this.settings.s3.filepathMaxLength;
            if (maxLength && fullPath.length > maxLength) {
                // get extension. If no extension, will return last char (not a problem)
                const ext = fullPath.slice(fullPath.lastIndexOf('.'));
                fullPath = lodash.trimEnd(fullPath.substring(0, maxLength - ext.length), delimiter) + ext;
            }

            options.metadata = options.metadata || {};
            options.metadata['Content-Type'] = options.contentType;
            if (options.public) {
                options.metadata['x-amz-acl'] = 'public-read';
                options.metadata['X-Amz-Meta-x-amz-acl'] ='public-read';
            }

            this.logger.info(`s3 (${container}): file upload ${fullPath}`, options);

            return client.putObject(
                container,
                fullPath,
                stream,
                options.size,
                options.metadata
            ).then(() => {
                return this._s3stat(fullPath, options);
            }).then((fstat) => {
                fstat.originalName = options.name;
                return fstat;
            });
        },

        _s3copy(source, destination, options = {}) {
            const delimiter = this._s3getDelimiter(options.client);
            const client = this._s3getClient(options.client);
            const container = options.container || this._s3getContainer(options.client);

            // copy can be done cross bucket. We need to reflect this here
            const sourceBucket = options.source_container || container;

            source = this._s3sanitizePath(source, options);
            destination = this._s3sanitizePath(destination, options);

            source = this._s3sanitizePath(sourceBucket + delimiter + source, options);

            this.logger.info(`s3 (${container}): file copy - source: ${source}, destination: ${destination}`);
            return client.copyObject(container, destination, source);
        },

        _s3getStreamForObject(name, offset = null, length = null, options = {}) {
            const client = this._s3getClient(options.client);
            const container = options.container || this._s3getContainer(options.client);

            name = this._s3sanitizePath(name, options);

            if (offset !== null && length !== null) {
                return client.getPartialObject(container, name, offset, length);
            }

            return client.getObject(container, name);
        },

        _s3remove(name, options = {}) {
            const client = this._s3getClient(options.client);
            const container = options.container || this._s3getContainer(options.client);

            const objName = this._s3sanitizePath(name, options);

            this.logger.info(`s3 (${container}): file remove: ${objName}`);
            return client.removeObject(container, objName);
        },

        _s3stat(name, options = {}) {
            const client = this._s3getClient(options.client);
            const container = options.container || this._s3getContainer(options.client);

            name = this._s3sanitizePath(name, options);

            this.logger.info(`s3 (${container}): file stat: ${name}`);
            return client.statObject(container, name)
                .then(fstat => {
                    return {
                        name: name,
                        size: fstat.size,
                        contentType: fstat.metaData['content-type'],
                        extension: this.mime.getExtension(fstat.metaData['content-type']),
                        lastModified: fstat.lastModified,
                        etag: fstat.etag,
                        metaData: fstat.metaData
                    };
                });
        },

        /**
         * Handle object store errors and convert them to Moleculer errors
         *
         * @param {Object} err
         * @throws MoleculerServerError
         */
        _s3handleUpstreamErrors(objName, options = {}) {
            return (err) => {
                objName = this._s3sanitizePath(objName || '');
                const client = options.client || this.s3defaultClient;
                switch (err.code) {
                    case 'NotFound':
                        if (!options.silent) {
                            this.logger.warn(`Object not found in client ${client}: ${objName}`, { err, objName });
                        }
                        throw new MoleculerServerError('Object not found', 404, 'ERR_NOT_FOUND', objName);

                    case 'AccessDenied':
                        if (!options.silent) {
                            this.logger.error(`Object store access denied for client ${client}`, err);
                        }
                        throw new MoleculerServerError('Object store access denied', 500, 'ERR_OS_ACCESS');

                    default:
                        if (!options.slient) {
                            this.logger.error(`Unhandled error from object storage for client ${client}`, err);
                        }
                        throw new MoleculerServerError(err.message, 500, err.code);
                }
            };
        }
    },

    async started() {
        this.settings.s3.filepathMaxLength = this.settings.s3.filepathMaxLength && Number(this.settings.s3.filepathMaxLength);

        this.s3clients = {};
        this.s3containers = {};
        this.s3delimiters = {};
        this.s3defaultClient = 'main';

        this.s3addClient(this.s3defaultClient, this.settings.s3);

        this.mime = (await mimeESM()).default;
    },

    stopped() {
        this.mime = undefined;
    }
};
